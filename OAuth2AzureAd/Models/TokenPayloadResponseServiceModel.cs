﻿namespace OAuth2AzureAd.Models
{
    public class TokenPayloadResponseServiceModel<T>
    {
        public bool IsTokenValid { get; set; } = true;
        public bool HasTokenExpired { get; set; }
        public T? TokenPayload { get; set; }
    }
}
