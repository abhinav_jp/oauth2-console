﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace OAuth2AzureAd.Models
{
    public class TokenClaimsResponseServiceModel
    {
        public bool IsTokenValid { get; set; } = true;
        public bool HasTokenExpired { get; set; }
        public List<Claim>? Claims { get; set; }
        public JwtPayload? Payload { get; set; }
    }
}
