﻿using Newtonsoft.Json;

namespace OAuth2AzureAd.Models
{
    public class AzureAdTokenPayload
    {

        [JsonProperty("preferred_username")]
        public string? Username { get; set; }
    }
}
