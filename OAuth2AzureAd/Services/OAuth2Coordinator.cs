﻿using System.Security.Claims;
using OAuth2AzureAd.Interfaces;
using OAuth2AzureAd.Models;

namespace OAuth2AzureAd.Services
{
    public class OAuth2Coordinator
    {
        private readonly IOAuth2Service _oAuth2Service;
        public OAuth2Coordinator(OAuth2Strategy oAuth2Strategy)
        {
            _oAuth2Service = OAuth2Factory.GetOAuth2Service(oAuth2Strategy);
        }

        public string GetAuthorizeUrl()
        {
            var oAuth2Config = _oAuth2Service.GetOAuth2Configuration();
            return _oAuth2Service.GetAuthorizeUrl(oAuth2Config);
        }

        public TokenClaimsResponseServiceModel GetClaims(string? token)
        {
            return _oAuth2Service.GetClaims(token);
        }

        public TokenPayloadResponseServiceModel<T> GetPayload<T>(string? token)
        {
            var claimsResponse = GetClaims(token);
            return IdentityTokenGenerationService.GetPayloadFromClaims<T>(claimsResponse);
        }
    }
}
