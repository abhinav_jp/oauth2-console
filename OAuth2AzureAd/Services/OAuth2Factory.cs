﻿using OAuth2AzureAd.Interfaces;
using OAuth2AzureAd.Models;

namespace OAuth2AzureAd.Services
{
    public static class OAuth2Factory
    {
        public static IOAuth2Service GetOAuth2Service(OAuth2Strategy oAuth2Strategy)
        {
            switch (oAuth2Strategy)
            {
                case OAuth2Strategy.AzureAd:
                default:
                    return new AzureAdOAuth2Service();
            }
        }
    }
}
