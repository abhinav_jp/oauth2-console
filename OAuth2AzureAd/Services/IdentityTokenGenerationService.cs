﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OAuth2AzureAd.Models;

namespace OAuth2AzureAd.Services
{
    public static class IdentityTokenGenerationService
    {
        public static TokenPayloadResponseServiceModel<T> GetPayload<T>(string? token)
        {
            var tokenDecodeResponse = GetTokenClaims(token);
            if (!tokenDecodeResponse.IsTokenValid || tokenDecodeResponse.HasTokenExpired)
            {
                return MapPayload<T>(tokenDecodeResponse, null);
            }

            return GetPayload<T>(tokenDecodeResponse);
        }

        public static TokenPayloadResponseServiceModel<T> GetPayloadFromClaims<T>(TokenClaimsResponseServiceModel tokenClaimsResponse)
        {
            if (!tokenClaimsResponse.IsTokenValid || tokenClaimsResponse.HasTokenExpired)
            {
                return MapPayload<T>(tokenClaimsResponse, null);
            }

            return GetPayload<T>(tokenClaimsResponse);
        }

        public static TokenClaimsResponseServiceModel GetTokenClaims(string? token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                throw new ArgumentException("Token isn't valid. Claims couldn't be fetched.");
            }
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var tokenDecodeResponse = new TokenClaimsResponseServiceModel();
            try
            {
                var decodedToken = jwtSecurityTokenHandler.ReadJwtToken(token);
                tokenDecodeResponse.Payload = decodedToken.Payload;

            }
            catch (Exception exception) when (
                            exception is SecurityTokenDecryptionFailedException
                                    or SecurityTokenInvalidAudienceException
                                    or SecurityTokenInvalidLifetimeException
                                    or SecurityTokenInvalidSignatureException
                                    or SecurityTokenNoExpirationException
                                    or SecurityTokenNotYetValidException
                                    or SecurityTokenReplayAddFailedException
                                    or SecurityTokenReplayDetectedException)


            {
                tokenDecodeResponse.IsTokenValid = false;
            }
            catch (Exception exception) when (
                exception is SecurityTokenExpiredException or SecurityTokenNoExpirationException)
            {
                tokenDecodeResponse.HasTokenExpired = true;
            }

            tokenDecodeResponse.Claims ??= new List<Claim>();
            return tokenDecodeResponse;
        }

        private static TokenPayloadResponseServiceModel<T> GetPayload<T>(TokenClaimsResponseServiceModel tokenClaimsResponse)
        {
            var jToken = GetPayloadAsJToken(tokenClaimsResponse);
            return MapPayload<T>(tokenClaimsResponse, jToken);
        }

        private static JToken GetPayloadAsJToken(TokenClaimsResponseServiceModel tokenClaimsResponse)
        {
            var jwtPayload = JsonConvert.SerializeObject(tokenClaimsResponse.Payload);
            var jToken = JToken.Parse(jwtPayload);
            return jToken;
        }

        private static TokenPayloadResponseServiceModel<T> MapPayload<T>(
            TokenClaimsResponseServiceModel tokenClaimsResponse, JToken? jTokenPayload)
        {
            return new TokenPayloadResponseServiceModel<T>
            {
                HasTokenExpired = tokenClaimsResponse.HasTokenExpired,
                IsTokenValid = tokenClaimsResponse.IsTokenValid,
                TokenPayload = jTokenPayload == null ? default : jTokenPayload.ToObject<T>()
            };
        }
    }
}
