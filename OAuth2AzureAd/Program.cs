﻿using OAuth2AzureAd.Models;
using OAuth2AzureAd.Services;

namespace OAuth2AzureAd;

internal class Program
{
    private static void Main()
    {
        var oAuth2Coordinator = new OAuth2Coordinator(OAuth2Strategy.AzureAd);
        var url = oAuth2Coordinator.GetAuthorizeUrl();

        Console.WriteLine("Please visit the following URL to sign in:");
        Console.WriteLine(url);
        Console.WriteLine();

        // Wait for user input after signing in
        Console.WriteLine("After signing in, you will be redirected to the redirect URI.");
        Console.WriteLine("Retrieve the ID token from the URL");

        // Retrieve the ID token from the redirect URI
        Console.WriteLine("Enter the ID token:");
        var idToken = Console.ReadLine();

        // Use the ID token to retrieve the user's email address

        var decodedToken = oAuth2Coordinator.GetPayload<AzureAdTokenPayload>(idToken);
        //var decodedToken = JWT.Decode<AzureAdTokenPayload>(idToken);
        if (decodedToken.HasTokenExpired)
        {
            Console.WriteLine("Token has expired");
            return;
        }
        if (!decodedToken.IsTokenValid)
        {
            Console.WriteLine("Token is not valid");
            return;
        }
        if (decodedToken.TokenPayload == null)
        {
            Console.WriteLine("Token payload is null");
            return;
        }

        var email = decodedToken.TokenPayload.Username;

        // Authenticate the user with your system using their email address
        // Replace this code with your authentication logic
        Console.WriteLine(!string.IsNullOrEmpty(email)
            ? $"User authenticated with email: {email}"
            : "Email address not found in the ID token. Authentication failed.");

        Console.WriteLine();
        Console.WriteLine("Press any key to exit.");
        Console.ReadKey();
    }
}