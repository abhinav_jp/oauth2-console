﻿using IdentityModel;
using IdentityModel.Client;
using OAuth2AzureAd.Interfaces;
using OAuth2AzureAd.Models;

namespace OAuth2AzureAd.Services
{
    public class AzureAdOAuth2Service : IOAuth2Service
    {
        public OAuth2Configuration GetOAuth2Configuration()
        {
            return new OAuth2Configuration
            {
                Authority = "https://login.microsoftonline.com/common",
                ClientId = "<client_id>",
                Scope = "openid",
                RedirectUri =
                    "https://www.google.com", // The redirect URI registered in your Azure AD application
            };
        }

        public string GetAuthorizeUrl(OAuth2Configuration azureAdConfig)
        {
            if (string.IsNullOrWhiteSpace(azureAdConfig.Authority))
            {
                throw new ArgumentException("Please give value", nameof(azureAdConfig.Authority));
            }

            if (string.IsNullOrWhiteSpace(azureAdConfig.ClientId))
            {
                throw new ArgumentException("Please give value", nameof(azureAdConfig.ClientId));
            }

            var authorityUri = new Uri(azureAdConfig.Authority);

            // Request an authorization code
            var authorizeRequest = new RequestUrl($"{authorityUri.AbsoluteUri}/oauth2/authorize");
            return authorizeRequest.CreateAuthorizeUrl(
                clientId: azureAdConfig.ClientId,
                responseType: OidcConstants.ResponseTypes.IdToken,
                scope: azureAdConfig.Scope,
                redirectUri: azureAdConfig.RedirectUri,
                state: Guid.NewGuid().ToString(),
                nonce: Guid.NewGuid().ToString());
        }

        public TokenClaimsResponseServiceModel GetClaims(string? token)
        {
            return IdentityTokenGenerationService.GetTokenClaims(token);
        }
    }
}
