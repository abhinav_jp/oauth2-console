﻿using System.Security.Claims;
using OAuth2AzureAd.Models;

namespace OAuth2AzureAd.Interfaces
{
    public interface IOAuth2Service
    {
        public OAuth2Configuration GetOAuth2Configuration();
        public string GetAuthorizeUrl(OAuth2Configuration azureAdConfig);
        public TokenClaimsResponseServiceModel GetClaims(string? token);
    }
}
