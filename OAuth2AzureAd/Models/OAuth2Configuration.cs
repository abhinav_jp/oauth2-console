﻿namespace OAuth2AzureAd.Models
{
    public class OAuth2Configuration
    {
        public string? Authority { get; set; }
        public string? ClientId { get; set; }
        public string? TenantId { get; set; }
        public string? ClientSecret { get; set; }
        public string? Scope { get; set; }
        public string? RedirectUri { get; set; }
    }
}
